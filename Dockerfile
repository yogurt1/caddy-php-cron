FROM abiosoft/caddy:builder as builder

RUN go get -v github.com/abiosoft/parent

ARG version="1.0.3"
ARG plugins="cors,realip,expires,cache,prometheus"

RUN VERSION=${version} PLUGINS=${plugins} ENABLE_TELEMETRY=false /bin/sh /usr/bin/builder.sh

FROM alpine:3.9

# ADD https://dl.bintray.com/php-alpine/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub
RUN set -eux ; \
  # (echo "https://dl.bintray.com/php-alpine/v3.8/php-7.2/" >> /etc/apk/repositories) && \
  apk add --no-cache \
  dcron \
  curl \
  ca-certificates \
  mailcap \
  tzdata \
  php7-fpm \
  php7-bcmath \
  php7-ctype \
  php7-curl \
  php7-dom \
  php7-exif \
  php7-fileinfo \
  php7-gd \
  php7-intl \
  php7-iconv \
  php7-json \
  php7-mbstring \
  php7-mysqli \
  php7-opcache \
  php7-openssl \
  php7-pdo \
  php7-pdo_mysql \
  php7-pdo_pgsql \
  php7-pdo_sqlite \
  php7-pgsql \
  php7-phar \
  php7-session \
  php7-simplexml \
  php7-sqlite3 \
  php7-tokenizer \
  php7-xml \
  php7-xmlreader \
  php7-xmlwriter \
  php7-zip

COPY misc/ /misc/
RUN /bin/sh /misc/postinstall.sh

# Let's Encrypt Agreement
ENV ACME_AGREE="false"
# Telemetry Stats
ENV ENABLE_TELEMETRY="false"

# Install Caddy
COPY --from=builder /install/caddy /usr/bin/caddy
COPY --from=builder /go/bin/parent /bin/parent

# Validate install
RUN set -e ; \
  caddy -version && \
  caddy -plugins

# 9280 - prometheus (check https://caddyserver.com/docs/http.prometheus)
# 2016 - php-fpm status page (/status)
EXPOSE 80 443 2015 9280 2016
VOLUME ["/root/.caddy", "/srv"]
WORKDIR /srv

ENTRYPOINT ["/bin/parent", "/usr/local/bin/docker-entrypoint.sh"]
