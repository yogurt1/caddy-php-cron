# caddy-php-cron

Based on [abiosoft/caddy-docker](https://github.com/abiosoft/caddy-docker) with extras:

- PHP-FPM 7 (`:php-no-stats` tag)
- Cron (`dcron`)
- Production config for PHP and PHP-FPM

## Command

- `caddy --conf /etc/Caddyfile` - caddy web server (default command)
- `cron` - start cron daemon
