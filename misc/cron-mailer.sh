#!/bin/sh
set -e

{
    echo "---CRON MAIL BEGIN---"
    cat /dev/stdin
    echo "---CRON MAIL END---"
} >> /run/cron.log

exit 0
