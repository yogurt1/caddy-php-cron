#!/bin/sh
set -e

# log fifo
mkfifo -m 0666 /run/cron.log

# start cron daemon
crond -b -M /usr/local/bin/cron-mailer.sh -L /run/cron.log

# get crond pid
CRONPID=$(cat /run/dcron.pid)

# trap SIGINT and SIGTERM signals and gracefully exit
trap "kill \$CRONPID; kill \$!; exit" INT TERM

# start "daemon"
while true
do
    # watch log restarting if necessary
    cat /run/cron.log & wait $!
done
