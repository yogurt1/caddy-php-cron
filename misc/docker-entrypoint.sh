#!/bin/sh

if [ "$1" == "cron" ]; then
    exec /usr/local/bin/cron.sh
fi

if [ "${1#-}" != "$1" ] || [ -z "$1" ]; then
    # first arg is `-f` or `--some-option`
	set -- caddy --conf /etc/Caddyfile "$@"
fi

exec "$@"
