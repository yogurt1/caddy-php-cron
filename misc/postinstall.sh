#!/bin/sh
set -e

# symlink php7 to php
ln -sf /usr/bin/php7 /usr/bin/php

# symlink php-fpm7 to php-fpm
ln -sf /usr/bin/php-fpm7 /usr/bin/php-fpm

# create www-data user and group
# 82 is the standard uid/gid for "www-data" in Alpine
# https://git.alpinelinux.org/aports/tree/main/apache2/apache2.pre-install?h=3.9-stable
# https://git.alpinelinux.org/aports/tree/main/lighttpd/lighttpd.pre-install?h=3.9-stable
# https://git.alpinelinux.org/aports/tree/main/nginx/nginx.pre-install?h=3.9-stable
addgroup -g 82 www-data
adduser -D -H -u 82 -G www-data www-data

# create /srv directory
install -m 644 /misc/index.php /srv/index.php
chown -R 82:82 /srv

# copy php-fpm config
install -m 644 /misc/php_fpm_docker.conf /etc/php7/php-fpm.d/zz-docker.conf

install -m 644 /misc/Caddyfile /etc/Caddyfile

# scripts
install -m755 /misc/cron.sh /usr/local/bin/cron.sh
install -m755 /misc/cron-mailer.sh /usr/local/bin/cron-mailer.sh
install -m755 /misc/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

# cleanup
rm -rf /misc
